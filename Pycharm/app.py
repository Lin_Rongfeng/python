from flask import Flask, render_template, request
from vsearch import search4letters

app = Flask(__name__)
# 中央管理中心是如何运转？
# 看左边的资源管理页面，一共有两个（entry，results）
# 再来看: 资源管理页面，一共有三个（index，entry，results）

@app.route('/')
def index_page():
    return render_template('index.html')

@app.route('/search4', methods=['POST'])
def do_search() -> str:
    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are your results: '
    results = str(search4letters(phrase, letters))
    return render_template('results.html', the_title=title, the_phrase=phrase, the_letters=letters, the_results=results)

@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html', the_title='Welcome to search4letters on the web!')

if __name__ == '__main__':
    app.run(debug=True)